
Transformers for wastewater treatment
======================================
The project explores `transformer <https://arxiv.org/abs/1706.03762>`_ based models for simulating removal
of heavy metals from wastewater using adsorption proecss. We will use two transformer based models i.e.
`Tab-Transformer <https://arxiv.org/abs/2012.06678>`_ and `FT-Transformer <https://arxiv.org/pdf/2106.11959.pdf>`_.
Other than using transformers for wastewater efficiency prediction, We will perform the following
tasks

 * how to perform feature selection for FT-Transformer model
 * Compare different validation schemes for model selection
 * Impact of logrithmic transformation on model prediction

The dataset initially consisted of 1514 samples and 28 features.

.. toctree::
   :maxdepth: 2
   :caption: Scripts

   auto_examples/index


.. toctree::
   :maxdepth: 2
   :caption: Notebooks

   recursive_feature_elimination.ipynb
