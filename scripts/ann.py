"""
============================
Artificial Neural Networks
============================
"""
import os


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from ai4water.utils.utils import TrainTestSplit
from ai4water.models import MLP
from ai4water import Model
from ai4water.utils import LossCurve
from utils import make_data, NUMERIC_FEATURES, CAT_FEATURES, LABEL

from utils import regression_plot, set_rcParams, residual_plot
from utils import evaluate_model, SAVE, maybe_save

set_rcParams()

# %%

data, _ = make_data(encoding="le")

splitter = TrainTestSplit(seed=142)

train_data, test_data, _, _ = splitter.split_by_random(data)
# %%

train_y = np.log(train_data[LABEL].values)
test_y = np.log(test_data[LABEL].values)

# %%
# All Inputs
# -----------
# model=Model(
#     model = MLP(units=14, num_layers=2, activation="relu"),
#     lr=0.001,
#     input_features=NUMERIC_FEATURES+CAT_FEATURES,
#     output_features=LABEL,
#     epochs=1000, batch_size=32,
#     verbosity=1
# )
m_path = os.path.join(os.getcwd(), "results", "ann_all_inputs_log")
c_path = os.path.join(m_path, "config.json")
model = Model.from_config_file(c_path)

# %%
#
# model.fit(
#     train_data[NUMERIC_FEATURES+CAT_FEATURES], train_y,
#     validation_data=(test_data[NUMERIC_FEATURES+CAT_FEATURES], test_y,
#                      )
# )
w_path = os.path.join(m_path, "weights", "weights_589_0.68566.hdf5")
model.update_weights(w_path)

# %%
train_p = model.predict(x=train_data[NUMERIC_FEATURES+CAT_FEATURES])
# %%
evaluate_model(train_y, train_p)


# %%
test_p = model.predict(x=test_data[NUMERIC_FEATURES+CAT_FEATURES])
# %%
evaluate_model(test_y, test_p)


# %%
# With Selected Inputs
# -----------------------

selected_features = ['H', 'H/C', 'O/C', 'Surface area',
                     'Adsorption_time (min)', 'Ci', 'solution pH',
 'Volume (L)', 'loading (g)', 'Ion Concentration (M)', 'Adsorbent',
 'Feedstock', 'inorganics', 'Anion_type']

num_features = [feature for feature in NUMERIC_FEATURES if feature in selected_features]
cat_features = [feature for feature in CAT_FEATURES if feature in selected_features]

# %%

train_x = train_data[selected_features].values
test_x = test_data[selected_features].values

# %%
#
# model=Model(
#     model = MLP(units=14, num_layers=2, activation="relu"),
#     lr=0.01,
#     input_features=selected_features,
#     output_features=LABEL,
#     epochs=500, batch_size=32,
#     verbosity=1
# )

m_path = os.path.join(os.getcwd(), "results", "ann_selected_inputs_log")
c_path = os.path.join(m_path, "config.json")
model = Model.from_config_file(c_path)

# %%
#
# model.fit(
#     train_x, train_y,
#     validation_data=(test_x, test_y,
#                      )
# )

w_path = os.path.join(m_path, "weights", "weights_469_1.08705.hdf5")
model.update_weights(w_path)

#%%%

train_p = model.predict(x=train_x)

#%%
evaluate_model(train_y, train_p)


test_p = model.predict(x=test_x)

# %%
evaluate_model(test_y, test_p)

# %%

ax = regression_plot(
    train_true=train_y,
    train_pred=train_p,
    test_true=test_y,
    test_pred=test_p,
    label="Adsorption Capacity (mg/g)"
)

# %%

residual_plot(
    train_true=train_y,
    train_prediction=train_p,
    test_true=test_y,
    test_prediction=test_p
)
if SAVE:
    plt.savefig("results/figures/ann_selected_inputs_log_res.png", dpi=600, bbox_inches="tight")
plt.show()

# %%

#ax.set_xlim(-35, right=ax.get_xlim()[1])
#ax.set_ylim(-35, top=ax.get_ylim()[1])
if SAVE:
    plt.savefig("results/figures/ann_selected_inputs_log_reg.png", dpi=600, bbox_inches="tight")
plt.show()
