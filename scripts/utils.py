"""
======
utils
======
"""

import warnings
from typing import Union, Any

import seaborn as sns

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.colors as mcolors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from easy_mpl import scatter, pie, regplot, plot, hist, bar_chart
from easy_mpl.utils import AddMarginalPlots, kde  # to add marginal plots along an axes

import shap
import pandas as pd

from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import numpy as np
import os
from ai4water.preprocessing import DataSet
from ai4water import Model
from SeqMetrics import RegressionMetrics

from ai4water.utils.utils import get_version_info
from ai4water.postprocessing import prediction_distribution_plot
from easy_mpl import violin_plot, boxplot

from ai4water.models import FTTransformer
from ai4water.utils.utils import TrainTestSplit
from ai4water.models.utils import gen_cat_vocab

# %%
SAVE = False

# %%
NUMERIC_FEATURES = ["Pyrolysis_temp", "Heating rate (oC)",
       "Pyrolysis_time (min)", "C", "H", "O", "N", "Ash", "H/C", "O/C", "N/C",
       "(O+N/C)", "Surface area", "Pore volume", "Average pore size",
        "Adsorption_time (min)", "Ci", "solution pH", "rpm",
       "Volume (L)", "loading (g)", "adsorption_temp",
                    "Ion Concentration (M)", "DOM"]
CAT_FEATURES = ["Adsorbent", "Feedstock", "inorganics", "Anion_type"]
LABEL = "qe"

# %%

CATEGORIES = {
    'Adsorption Conditions':
        ["Adsorption_time (min)", "Ci", "solution pH", "rpm",
         "Volume (L)", "loading (g)", "adsorption_temp",
         "Ion Concentration (M)", "DOM", "inorganics", "Anion_type"],
    'Physical Properties':
        ["Surface area", "Pore volume", "Average pore size"],
    'Synthesis Conditions':
        ["Pyrolysis_temp", "Heating rate (oC)", "Pyrolysis_time (min)",
         "Adsorbent", "Feedstock"],
    'Adsorbent Composition':
        ["C", "H", "O", "N", "Ash", "H/C", "O/C", "N/C", "(O+N/C)"],
          }

# %%

LABEL_MAP = {
    "Feedstock": "Feedstocks",
    "Ci": "Ci",
    "Adsorbent": "Adsorbents",
    "loading (g)": "Loading (g)",
    "solution pH": "Solution pH",
    "inorganics": "Heavy Metal Ions"
}

# %%
def maybe_save(fpath):
    if SAVE:
        plt.savefig(fpath,
                    dpi=600,
                    bbox_inches="tight")
    return

def _load_data(input_features=None):
    # read excell file
    dirname = os.path.dirname(__file__)
    data = pd.read_excel(os.path.join(dirname, 'HMI_data.xlsx'), sheet_name=0)

    # default inputs
    def_inputs = ['Adsorbent', 'Feedstock', 'Pyrolysis_temp', 'Heating rate (oC)',
           'Pyrolysis_time (min)', 'C', 'H', 'O', 'N', 'Ash', 'H/C', 'O/C', 'N/C',
           '(O+N/C)', 'Surface area', 'Pore volume', 'Average pore size',
           'inorganics',
                    'Adsorption_time (min)', 'Ci', 'solution pH', 'rpm',
           'Volume (L)', 'loading (g)', 'g/L', 'adsorption_temp',
           'Ion Concentration (M)', 'Anion_type', 'DOM', 'Cf']

    data.columns = ['Adsorbent', 'Feedstock', 'Pyrolysis_temp', 'Heating rate (oC)',
           'Pyrolysis_time (min)', 'C', 'H', 'O', 'N', 'Ash', 'H/C', 'O/C', 'N/C',
           '(O+N/C)', 'Surface area', 'Pore volume', 'Average pore size',
           'inorganics',
                    'radius (pm)', 'hydra_radius_pm', 'First_ionic_IE_KJ/mol',
                    'Adsorption_time (min)', 'Ci', 'solution pH', 'rpm',
           'Volume (L)', 'loading (g)', 'g/L', 'adsorption_temp',
           'Ion Concentration (M)', 'Anion_type', 'DOM', 'Cf', 'qe']

    target = ['qe']

    if input_features is None:
        input_features = def_inputs
    else:
        assert isinstance(input_features, list)
        assert all([feature in data.columns for feature in input_features])


    return data[input_features + target]


def _ohe_column(df, col_name:str):


        encoder = OneHotEncoder(sparse=False)
        ohe_cat = encoder.fit_transform(df[col_name].values.reshape(-1,1))
        cols_added = [f"{col_name}_{i}" for i in range (ohe_cat.shape[-1])]

        df[cols_added] = ohe_cat
        df.pop(col_name)
        return df, cols_added, encoder


def le_column(df:pd.DataFrame, col_name)->tuple:
    """label encode a column in dataframe"""
    encoder = LabelEncoder()
    df[col_name] = encoder.fit_transform(df[col_name])
    return df, encoder


def make_data(
        inputs = None,
        encoding = "ohe"):
    data = _load_data(input_features=inputs)

    encoders = {}

    if encoding == "ohe":
        for arg in ['Adsorbent', 'Feedstock', 'inorganics', 'Anion_type']:
            if arg in data:
                data, _, encoders[arg] = _ohe_column(data, arg)
    elif encoding == "le":
        for arg in ['Adsorbent', 'Feedstock', 'inorganics', 'Anion_type']:
            if arg in data:
                data,encoders[arg] = le_column(data, arg)


    # moving target to the last
    target = data.pop('qe')
    data['qe'] = target

    return data, encoders


def get_dataset(encoding="ohe"):

    data, encoders = make_data(
        encoding=encoding
    )

    dataset = DataSet(data=data,
                      seed=1000,
                      val_fraction=0.0,
                      train_fraction=0.7,
                      split_random=True,
                      input_features=data.columns.tolist()[0:-1],
                      output_features=data.columns.tolist()[-1:],
                      )

    return dataset, encoders


def set_rcParams(**kwargs):
    # https://matplotlib.org/stable/tutorials/introductory/customizing.html
    _kwargs = {
        'axes.labelsize': '14',
        'axes.labelweight': 'bold',
        'xtick.labelsize': '12',
        'ytick.labelsize': '12',
        'font.weight': 'bold',
        'legend.title_fontsize': '12',
        'axes.titleweight': 'bold',
        'axes.titlesize': '14',
        "font.family" : "Times New Roman"
    }

    if kwargs:
        _kwargs.update(kwargs)

    for k,v in _kwargs.items():
        plt.rcParams[k] = v

    return

# %%

def version_info()->dict:
    info = get_version_info()
    info['shap'] = shap.__version__
    return info

# %%

def evaluate_model(true, predicted):
    metrics = RegressionMetrics(true, predicted)
    for i in ['mse', 'rmse', 'r2_score', 'mape', 'mae', 'r2']:
        print(i, getattr(metrics, i)())
    return

# %%

def residual_plot(
        train_true,
        train_prediction,
        test_true,
        test_prediction,
        label="Prediction",
        train_color="orange",
        test_color="royalblue",
        show:bool = False
):
    fig, axis = plt.subplots(1, 2, sharey="all"
                             , gridspec_kw={'width_ratios': [2, 1]})
    test_y = test_true.reshape(-1, ) - test_prediction.reshape(-1, )
    train_y = train_true.reshape(-1, ) - train_prediction.reshape(-1, )
    train_hist_kws = dict(bins=20, linewidth=0.5,
                          edgecolor="k", grid=False, color=train_color,  # "#009E73"
                          orientation='horizontal')
    hist(train_y, show=False, ax=axis[1],
         label="Training", **train_hist_kws)
    plot(train_prediction, train_y, 'o', show=False,
         ax=axis[0],
         color=train_color,
         markerfacecolor=train_color,
         markeredgecolor="black", markeredgewidth=0.5,
         alpha=0.7, label="Training"
         )

    #****#
    test_hist_kws = dict(bins=40, linewidth=0.5,
                     edgecolor="k", grid=False,
                     color=test_color,
                     orientation='horizontal')
    hist(test_y, show=False, ax=axis[1],
         **test_hist_kws)

    set_xticklabels(axis[1], 3)

    plot(test_prediction, test_y, 'o', show=False,
         ax=axis[0],
         color=test_color,
         markerfacecolor=test_color,
         markeredgecolor="black", markeredgewidth=0.5,
         ax_kws=dict(
             xlabel=label,
             ylabel="Residual",
             legend_kws=dict(loc="upper left"),
         ),
         alpha=0.7, label="Test",
         )
    set_xticklabels(axis[0], 5)
    set_yticklabels(axis[0], 5)
    axis[0].axhline(0.0, color="black")
    plt.subplots_adjust(wspace=0.15)

    if show:
       plt.show()
    return

def regression_plot(
        train_true,
        train_pred,
        test_true,
        test_pred,
        label='',
        max_xtick_val = None,
        max_ytick_val = None,
        min_xtick_val=None,
        min_ytick_val=None,
        max_ticks = 5,
        show=False,
        train_color = "orange",
        test_color= "royalblue",
):
    TRAIN_RIDGE_LINE_KWS = [{'color': "black", 'lw': 1.0},
                            {'color': "black", 'lw': 1.0}]
    TRAIN_HIST_KWS = [{'color': train_color, 'bins': 50, 'alpha': 0.6},
                      {'color': train_color, 'bins': 50, 'alpha': 0.6}]

    train_fill_kws = [{'color': train_color}, {'color': train_color}]
    test_fill_kws = [{'color': test_color}, {'color': test_color}]

    ax = regplot(train_true, train_pred,
                 marker_size=35,
                 marker_color=train_color,
                 line_color='k',
                 fill_color='k',
                 scatter_kws={
                     'edgecolors': 'black',
                              'linewidth': 0.5,
                              'alpha': 0.5,
                              },
                 label="Training",
                 show=False
                 )

    class MarginPlotter(AddMarginalPlots):

        def _add_ridge_on_top(self, ax2: plt.Axes, data, cut=0.2, **line_kws):
            ind, density = None, None
            if self.ridge:

                whole_weight = len(train_true) + len(test_true)
                part_weight = len(data)
                ind, density = kde(data, cut=cut)

                density *= part_weight / whole_weight

                ax2.plot(ind, density, **line_kws)

            return ind, density

        def _add_ridge_on_side(self, ax: plt.Axes, data, cut=0.2, **line_kws):
            ind, density = None, None
            if self.ridge:

                whole_weight = len(train_true) + len(test_true)
                part_weight = len(data)

                ind, density = kde(data, cut=cut)

                density *= part_weight / whole_weight

                ax.plot(density, ind, **line_kws)
            return ind, density

    axHistx, axHisty = MarginPlotter(
        ax,
        ridge=True,
        hist=False,
        pad=0.25,
        size=0.7,
        ridge_line_kws=TRAIN_RIDGE_LINE_KWS,
        hist_kws=TRAIN_HIST_KWS,
        fill_kws=train_fill_kws,
    )(train_true, train_pred)

    train_r2 = RegressionMetrics(train_true, train_pred).r2()
    test_r2 = RegressionMetrics(test_true, test_pred).r2()
    ax.annotate(f'Training $R^2$= {round(train_r2, 2)}',
                xy=(0.95, 0.30),
                xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='top',
                fontsize=12, weight="bold")
    ax.annotate(f'Test $R^2$= {round(test_r2, 2)}',
                xy=(0.95, 0.20),
                xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='top',
                fontsize=12, weight="bold")

    ax_ = regplot(test_true, test_pred,
                  marker_size=35,
                  marker_color=test_color,
                  line_style=None,
                  scatter_kws={
                      'edgecolors': 'black',
                               'linewidth': 0.5,
                               'alpha': 0.5,
                               },
                  show=False,
                  label="Test",
                  ax=ax
                  )

    ax_.legend(fontsize=12, prop=dict(weight="bold"))
    TEST_RIDGE_LINE_KWS = [{'color': "black", 'lw': 1.0},
                           {'color': "black", 'lw': 1.0}]
    TEST_HIST_KWS = [{'color': test_color, 'bins': 50, 'alpha': 0.6},
                     {'color': test_color, 'bins': 50, 'alpha': 0.6}]
    MarginPlotter(
        ax,
        ridge=True,
        hist=False,
        pad=0.25,
        size=0.7,
        ridge_line_kws=TEST_RIDGE_LINE_KWS,
        hist_kws=TEST_HIST_KWS,
        fill_kws=test_fill_kws,
    )(test_true, test_pred, axHistx, axHisty)

    set_xticklabels(
        ax_,
        max_xtick_val=max_xtick_val,
        min_xtick_val=min_xtick_val,
        max_ticks=max_ticks,
    )
    set_yticklabels(
        ax_,
        max_ytick_val=max_ytick_val,
        min_ytick_val=min_ytick_val,
        max_ticks=max_ticks
    )
    ax.set_xlabel(f"Experimental {label}")
    ax.set_ylabel(f"Predicted {label}")

    if show:
        plt.show()
    return ax

# %%

def set_xticklabels(
        ax:plt.Axes,
        max_ticks:Union[int, Any] = 5,
        dtype = int,
        weight = "bold",
        fontsize:Union[int, float]=12,
        max_xtick_val=None,
        min_xtick_val=None,
        **kwargs
):
    """

    :param ax:
    :param max_ticks:
        maximum number of ticks, if not set, all the default ticks will be used
    :param dtype:
    :param weight:
    :param fontsize:
    :param max_xtick_val:
        maxikum value of tick
    :param min_xtick_val:
    :return:
    """
    return set_ticklabels(ax, "x", max_ticks, dtype, weight, fontsize,
                          max_tick_val=max_xtick_val,
                          min_tick_val=min_xtick_val,
                          **kwargs)


def set_yticklabels(
        ax:plt.Axes,
        max_ticks:Union[int, Any] = 5,
        dtype=int,
        weight="bold",
        fontsize:int=12,
        max_ytick_val = None,
        min_ytick_val = None,
        **kwargs
):
    return set_ticklabels(
        ax, "y", max_ticks, dtype, weight,
        fontsize=fontsize,
        max_tick_val=max_ytick_val,
        min_tick_val=min_ytick_val,
        **kwargs
    )


def set_ticklabels(
        ax:plt.Axes,
        which:str = "x",
        max_ticks:int = 5,
        dtype=int,
        weight="bold",
        fontsize:int=12,
        max_tick_val = None,
        min_tick_val = None,
        **kwargs
):
    """

    :param ax:
    :param which:
    :param max_ticks:
    :param dtype:
    :param weight:
    :param fontsize:
    :param max_tick_val:
    :param min_tick_val:
    :param kwargs:
        any keyword arguments of axes.set_{x/y}ticklabels()
    :return:
    """
    ticks_ = getattr(ax, f"get_{which}ticks")()
    ticks = np.array(ticks_)
    if len(ticks)<1:
        warnings.warn(f"can not get {which}ticks {ticks_}")
        return

    if max_ticks:
        ticks = np.linspace(min_tick_val or min(ticks), max_tick_val or max(ticks), max_ticks)

    ticks = ticks.astype(dtype)

    getattr(ax, f"set_{which}ticks")(ticks)

    getattr(ax, f"set_{which}ticklabels")(ticks,
                                          weight=weight,
                                          fontsize=fontsize,
                                          **kwargs
                                          )
    return ax

# %%

def bar_pie(
        data:pd.DataFrame,
        ax:plt.Axes=None,
        save:bool = True,
        name:str = '',
        show:bool = True,
):
    if ax is None:
        f, ax = plt.subplots(figsize=(7, 9))

    sv_bar = data['mean_shap'].copy()
    colors = data['colors'].unique()
    feature_names = data['features'].tolist()

    ax_ = bar_chart(
        sv_bar,
        [LABEL_MAP[n] if n in LABEL_MAP else n for n in feature_names],
        bar_labels=np.round(sv_bar, 4),
        bar_label_kws={'label_type': 'edge',
                       'fontsize': 10,
                       'weight': 'bold',
                       "fmt": '%.4f',
                       'padding': 1.5
                       },
        show=False,
        sort=True,
        color=data['colors'].to_list(),
        ax=ax
    )
    ax_.spines[['top', 'right']].set_visible(False)
    ax_.set_xlabel(xlabel='mean(|SHAP value|)')
    # ax.set_xticklabels(ax.get_xticks().astype(float))
    ax_.set_yticklabels(ax_.get_yticklabels())

    labels = data['classes'].unique()
    handles = [plt.Rectangle((0, 0), 1, 1,
                             color=colors[idx]) for idx, l in enumerate(labels)]
    ax_.legend(handles, labels, loc='lower right')
    ax_.xaxis.set_major_locator(plt.MaxNLocator(4))

    # %%

    seg_colors = tuple(colors)
    # Change the saturation of seg_colors to 70% for the interior segments
    rgb = mcolors.to_rgba_array(seg_colors)[:, :-1]
    hsv = mcolors.rgb_to_hsv(rgb)
    hsv[:, 1] = 0.95 * hsv[:, 1]
    interior_colors = mcolors.hsv_to_rgb(hsv)

    labels = data['classes'].unique().tolist()

    fractions = []
    for label in labels:
        fractions.append(data.loc[data['classes'] == label]['mean_shap'].sum())

    fractions = np.array(fractions)

    fractions /= fractions.sum()

    for label, fraction in zip(labels, fractions):
        print(label, fraction)

    ax2 = inset_axes(ax, width='50%', height='50%',
                     loc=5)

    _, texts = pie(fractions=fractions,
                   colors=seg_colors,
                   #labels=labels,
                   wedgeprops=dict(edgecolor="w", width=0.03),
                   radius=1,
                   autopct=None,
                   textprops=dict(fontsize=12),
                   startangle=90, counterclock=False, show=False,
                   ax=ax2)
    #texts[0].set_fontsize(12)
    _, texts = pie(fractions=fractions,
                              colors=interior_colors,
                              autopct=None,
                              textprops=dict(fontsize=24),
                              wedgeprops=dict(edgecolor="w"),
                              radius=1 - 2 * 0.03,
                              startangle=90,
                              counterclock=False, ax=ax2, show=False)
    #texts[0].set_fontsize(12)
    if save:
        plt.savefig(f"results/figures/shap_bar_{name}.png",
                    dpi=600,
                    bbox_inches="tight")

    if show:
        plt.tight_layout()
        plt.show()
    return

# %%

def shap_scatter(
        feature_shap_values:np.ndarray,
        feature_data:Union[pd.DataFrame, np.ndarray, pd.Series],
        color_feature:pd.Series=None,
        color_feature_is_categorical:bool = False,
        feature_name:str = '',
        show_hist:bool = True,
        palette_name = "tab10",
        s:int = 70,
        ax:plt.Axes = None,
        edgecolors='black',
        linewidth=0.8,
        alpha=0.8,
        show:bool = True,
        **scatter_kws,
):
    """

    :param feature_shap_values:
    :param feature_data:
    :param color_feature:
    :param color_feature_is_categorical:
        whether the color feautre is categorical or not. If categorical then the
        array ``color_feature`` is supposed to contain categorical (either string or numerical) values which
        are then mapped to the color and are used prepare the legend box.
    :param feature_name:
    :param show_hist:
    :param palette_name:
        only relevant if ``color_feature_is_categorical`` is True
    :param s:
    :param ax:
    :param edgecolors:
    :param linewidth:
    :param alpha:
    :param show:
    :param scatter_kws:
    :return:
    """
    if ax is None:
        fig, ax = plt.subplots()

    if color_feature is None:
        c = None
    else:
        if color_feature_is_categorical:
            if isinstance(palette_name, (tuple, list)):
                assert len(palette_name) == len(color_feature.unique())
                rgb_values = palette_name
            else:
                rgb_values = sns.color_palette(palette_name, color_feature.unique().__len__())
            color_map = dict(zip(color_feature.unique(), rgb_values))
            c= color_feature.map(color_map)
        else:
            c = color_feature.values.reshape(-1,)

    _, pc = scatter(
        feature_data,
        feature_shap_values,
        c=c,
        s=s,
        marker="o",
        edgecolors=edgecolors,
        linewidth=linewidth,
        alpha=alpha,
        ax=ax,
        show=False,
        **scatter_kws
    )

    if color_feature is not None:
        feature_wrt_name = ' '.join(color_feature.name.split('_'))
        if color_feature_is_categorical:
            # add a legend
            handles = [Line2D([0], [0],
                              marker='o',
                              color='w',
                              markerfacecolor=v,
                              label=k, markersize=8) for k, v in color_map.items()]

            ax.legend(title=feature_wrt_name,
                  handles=handles, bbox_to_anchor=(1.05, 1),
                      loc='upper left',
                      title_fontsize=14
                      )
        else:
            fig = ax.get_figure()
            # increasing aspect will make the colorbar thin
            cbar = fig.colorbar(pc, ax=ax, aspect=20)
            cbar.ax.set_ylabel(feature_wrt_name,
                               rotation=90, labelpad=14)

            cbar.set_alpha(1)
            cbar.outline.set_visible(False)

    feature_name = LABEL_MAP.get(feature_name, feature_name)
    ax.set_xlabel(feature_name)
    ax.set_ylabel(f"SHAP value for {feature_name}")
    ax.axhline(0, color='grey', linewidth=1.3, alpha=0.3, linestyle='--')

    if show_hist:
        if isinstance(feature_data, (pd.Series, pd.DataFrame)):
            feature_data = feature_data.values
        x = feature_data

        if len(x) >= 500:
            bin_edges = 50
        elif len(x) >= 200:
            bin_edges = 20
        elif len(x) >= 100:
            bin_edges = 10
        else:
            bin_edges = 5

        ax2 = ax.twinx()

        xlim = ax.get_xlim()

        ax2.hist(x.reshape(-1,), bin_edges,
                 range=(xlim[0], xlim[1]),
                 density=False, facecolor='#000000', alpha=0.1, zorder=-1)
        ax2.set_ylim(0, len(x))
        ax2.set_yticks([])

    if show:
        plt.tight_layout()
        plt.show()

    return ax


# %%

def make_model(X, hidden_units, num_heads):

    cat_available = [feat for feat in CAT_FEATURES if feat in X.columns]
    num_available = [feat for feat in NUMERIC_FEATURES if feat in X.columns]
    cat = X.loc[:, cat_available].astype(int).astype(str)
    num = X.loc[:, num_available]

    cat_vocab = gen_cat_vocab(cat)
    model_ = Model(model=FTTransformer(len(num_available), cat_vocab,
                                      hidden_units=hidden_units,
                                       num_heads=num_heads
                                       ),
                   patience=50,
                   verbosity=0)
    return model_, [num, cat]

# %%

def feature_importance(
        estimator,
        X,):

    w = estimator.get_fttransformer_weights(X)

    feature_importances_ = w.mean(axis=0)

    return feature_importances_

# %%

def train(estimator, X, y, epochs, **fit_params):

    splitter = TrainTestSplit(seed=1000)
    train_X, test_X, train_y, test_y = splitter.split_by_random(X, y)

    estimator.fit(train_X, train_y,
                  validation_data=(test_X, test_y),
                  epochs=epochs,
                  **fit_params)
    return estimator, [test_X, test_y]
