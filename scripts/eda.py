"""
==========================
Exploratory Data Analysis
==========================
"""

from ai4water.eda import EDA

import matplotlib.pyplot as plt

from easy_mpl import pie

from utils import make_data, set_rcParams, maybe_save

# %%
set_rcParams()

# %%

data, encoders = make_data(encoding=None)

# %%

print(data.shape)

# %%

data.describe()

# %%
eda = EDA(data = data, save=False, show=False)

# %%
# Pearson Correlation matrix

ax = eda.correlation(figsize=(9,9), annot=True, fmt=".1f",
                     annot_kws={"size": 9}, cmap="flare")
ax.set_xticklabels(ax.get_xticklabels(), fontsize=12, weight='bold')
ax.set_yticklabels(ax.get_yticklabels(), fontsize=12, weight='bold')
plt.tight_layout()
maybe_save("results/figures/correlation.png")
plt.show()

# %%

pie(data['Anion_type'])

# %%

pie(data['Adsorbent'])

# %%

pie(data['Feedstock'])

# %%

pie(data['inorganics'])

# %%