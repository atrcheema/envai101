"""
=========================
Interpretation
=========================
"""

import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from ai4water.functional import Model
from ai4water.utils.utils import TrainTestSplit
from ai4water.models.utils import gen_cat_vocab

from shap.plots import colors
from shap import KernelExplainer
from shap import Explanation

from easy_mpl import imshow

from sklearn.preprocessing import LabelEncoder

from utils import SAVE, CATEGORIES
from utils import bar_pie, shap_scatter, maybe_save
from utils import make_data, version_info, set_rcParams

# %%

set_rcParams()

# %%

for k,v in version_info().items():
    print(k, v)

# %%

cmap = colors.red_blue
# %%

m_path = os.path.join(os.getcwd(), "results", "ft_transformer_selected_inputs_log")
c_path = os.path.join(m_path, "config.json")
w_path = os.path.join(m_path, "weights", "weights_110_0.08815.hdf5")

ftt_model = Model.from_config_file(c_path)

# %%

ftt_model.update_weights(w_path)

# %%

data, encoders = make_data(encoding=None)


NUMERIC_FEATURES = ["Pyrolysis_temp", "Heating rate (oC)",
       "Pyrolysis_time (min)", "C", "H", "O", "N", "Ash", "H/C", "O/C", "N/C",
       "(O+N/C)", "Surface area", "Pore volume", "Average pore size",
        "Adsorption_time (min)", "Ci", "solution pH", "rpm",
       "Volume (L)", "loading (g)", "adsorption_temp",
                    "Ion Concentration (M)", "DOM"]
CAT_FEATURES = ["Adsorbent", "Feedstock", "inorganics", "Anion_type"]
LABEL = "qe"

# %%

splitter = TrainTestSplit(seed=1000)

data[NUMERIC_FEATURES] = data[NUMERIC_FEATURES].astype(float)
data[CAT_FEATURES] = data[CAT_FEATURES].astype(str)
data['qe'] = data['qe'].astype(float)

train_data, test_data, _, _ = splitter.split_by_random(data)

# create vocabulary of unique values of categorical features
cat_vocabulary = gen_cat_vocab(data)

train_y = np.log(train_data[LABEL].values)
test_y = np.log(test_data[LABEL].values)

# %%

selected_features = ['H', 'H/C', 'O/C', 'Surface area',
                     'Adsorption_time (min)', 'Ci', 'solution pH',
 'Volume (L)', 'loading (g)', 'Ion Concentration (M)', 'Adsorbent',
 'Feedstock', 'inorganics', 'Anion_type']

num_features = [feature for feature in NUMERIC_FEATURES if feature in selected_features]
cat_features = [feature for feature in CAT_FEATURES if feature in selected_features]

# %%

train_x = [train_data[num_features].values, train_data[cat_features].values]
test_x = [test_data[num_features].values, test_data[cat_features].values]

# %%

ads_encoder = LabelEncoder()
ads_le = ads_encoder.fit_transform(train_data["Adsorbent"].values)
fs_encoder = LabelEncoder()
fs_le = fs_encoder.fit_transform(train_data["Feedstock"].values)
ino_encoder = LabelEncoder()
ino_le = ino_encoder.fit_transform(train_data["inorganics"].values)
ant_encoder = LabelEncoder()
ant_le = ant_encoder.fit_transform(train_data["Anion_type"].values)

cat_le_train = np.column_stack([ads_le, fs_le, ino_le, ant_le])

# %%

ads_encoder_test = LabelEncoder()
ads_le_test = ads_encoder_test.fit_transform(test_data["Adsorbent"].values)
fs_encoder_test = LabelEncoder()
fs_le_test = fs_encoder_test.fit_transform(test_data["Feedstock"].values)
ino_encoder_test = LabelEncoder()
ino_le_test = ino_encoder_test.fit_transform(test_data["inorganics"].values)
ant_encoder_test = LabelEncoder()
ant_le_test = ant_encoder_test.fit_transform(test_data["Anion_type"].values)

cat_le_test = np.column_stack([ads_le_test,
                               fs_le_test,
                               ino_le_test, ant_le_test])

# %%

x_train_all = pd.DataFrame(
    np.column_stack([train_data[num_features].values, cat_le_train]),
    columns=num_features + cat_features)

# %%

x_test_all = pd.DataFrame(
    np.column_stack([test_data[num_features].values, cat_le_test]),
    columns=num_features + CAT_FEATURES)

class SingleInputModel:

    def predict(self, X):


        if not isinstance(X, pd.DataFrame):
            X = pd.DataFrame(X, columns=num_features + cat_features)

        # decode categorical features
        ads = ads_encoder_test.inverse_transform(X.loc[:, 'Adsorbent'].values.astype(int))
        fs = fs_encoder_test.inverse_transform(X.loc[:, 'Feedstock'].values.astype(int))
        ino = ino_encoder_test.inverse_transform(X.loc[:, 'inorganics'].values.astype(int))
        ant = ant_encoder_test.inverse_transform(X.loc[:, 'Anion_type'].values.astype(int))

        cat_x = pd.DataFrame(
            np.column_stack([ads, fs, ino, ant]),
            columns=CAT_FEATURES, dtype=str)

        num_x = X.loc[:, num_features].astype(float)

        X = [num_x.values, cat_x.values]
        return np.exp(ftt_model.predict(x=X, verbose=0).reshape(-1,))

# %%

fname = "kernel_shap_ftt.csv"
fpath = os.path.join(os.getcwd(), "results", "figures", fname)

if os.path.exists(fpath):
    sv_df = pd.read_csv(fpath, index_col="Unnamed: 0")
else:
    exp = KernelExplainer(SingleInputModel().predict, x_train_all)
    sv = exp.shap_values(x_test_all, nsamples=200)

    sv_df = pd.DataFrame(sv, columns=num_features + cat_features)

    sv_df.to_csv(fpath)


# %%
shap_values_exp = Explanation(
    sv_df.values,
    data=test_x,
    feature_names=num_features + cat_features
)

# %%

sv_bar = np.mean(np.abs(sv_df.values), axis=0)

def make_classes(exp):
    colors = {'Adsorption Conditions': '#60AB7B',
          'Physical Properties': '#F9B234',
          'Synthesis Conditions': '#E91B23',
          'Adsorbent Composition': 'k',
          }

    classes = []
    colors_ = []
    for f in exp.feature_names:
        if f in CATEGORIES['Adsorption Conditions']:
            classes.append('Adsorption Conditions')
            colors_.append(colors['Adsorption Conditions'])
        elif f in CATEGORIES['Physical Properties']:
            classes.append('Physical Properties')
            colors_.append(colors['Physical Properties'])
        elif f in CATEGORIES['Synthesis Conditions']:
            classes.append('Synthesis Conditions')
            colors_.append(colors['Synthesis Conditions'])
        elif f in CATEGORIES['Adsorbent Composition']:
            classes.append('Adsorbent Composition')
            colors_.append(colors['Adsorbent Composition'])
        else:
            raise ValueError(f"{f} not found")

    return classes, colors_

classes, colors_ = make_classes(shap_values_exp)

# %%

df_with_classes = pd.DataFrame(
    {'features': shap_values_exp.feature_names,
     'classes': classes,
     'mean_shap': sv_bar,
     'colors': colors_
     })

print(df_with_classes)

# %%

bar_pie(df_with_classes,
         save=SAVE,
         name="ftt_kernel",
        show=True)

# %%


fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3,
                                                       figsize=(12, 8))

color_feature = 'O/C'

feature_name = 'loading (g)'

shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=x_test_all.loc[:, feature_name].values,
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    ax=ax1,
    show=False
)

feature_name = 'Adsorbent'
feature_data = pd.Series(ads_encoder_test.inverse_transform(
        x_test_all.loc[:, feature_name].values.astype(int)), name=feature_name)
ax = shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=feature_data,
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    ax=ax2,
    show=False
)
ax.set_xticklabels(feature_data.unique(), rotation=90)

feature_name = 'Ci'
shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=x_test_all.loc[:, feature_name].values,
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    ax=ax3,
    show=False
)


feature_name = 'inorganics'

feature_data = pd.Series(ino_encoder_test.inverse_transform(
        x_test_all.loc[:, feature_name].values.astype(int)),
    name=feature_name
)

ax = shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=feature_data,
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    ax=ax4,
    show=False
)
ax.set_xticklabels(feature_data.unique(), rotation=90)

feature_name = 'Feedstock'
feature_data = pd.Series(fs_encoder_test.inverse_transform(
        x_test_all.loc[:, feature_name].values.astype(int)),
        name=feature_name
    )
ax = shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=feature_data,
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    ax=ax5,
    show=False
)
ax.set_xticklabels(feature_data.unique(), rotation=90)

feature_name = 'solution pH'
shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=x_test_all.loc[:, feature_name],
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    ax=ax6,
    show=False
)

plt.tight_layout()
if SAVE:
    plt.savefig("results/figures/shap_interaction.png", dpi=600, bbox_inches="tight")
plt.show()

# %%

feature_name = 'inorganics'
color_feature = "solution pH"
feature_data = pd.Series(ino_encoder_test.inverse_transform(
        x_test_all.loc[:, feature_name].values.astype(int)),
    name=feature_name
)

ax = shap_scatter(
    feature_shap_values=sv_df.loc[:, feature_name].values,
    feature_data=feature_data,
    feature_name=feature_name,
    color_feature=x_test_all.loc[:, color_feature],
    cmap=cmap,
    show=False
)
ax.set_xticklabels(feature_data.unique(), rotation=90)

plt.tight_layout()
if SAVE:
    plt.savefig("results/figures/shap_interaction_inorg_ph.png", dpi=600, bbox_inches="tight")
plt.show()

# %%
imshow(sv_df.values, xticklabels=num_features + cat_features,
       aspect="auto",
       cmap="hot",
       show=False,
       colorbar=True)
plt.tight_layout()
plt.show()
