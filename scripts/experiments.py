"""
==============
Experiments
==============
"""


import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from utils import set_rcParams, NUMERIC_FEATURES, CAT_FEATURES
from utils import maybe_save
from utils import make_data, version_info
from ai4water.experiments import MLRegressionExperiments
from ai4water.utils.utils import jsonize, ERROR_LABELS

# %%
set_rcParams()

# %%

for k,v in version_info().items():
    print(k, v)

# %%

data, _ = make_data(encoding="ohe")

print(data.shape)

# %%

data.head()

# %%
# With all input features
# ----------------------------


comparisons = MLRegressionExperiments(
    input_features=data.columns.tolist()[0:-1],
    output_features=data.columns.tolist()[-1:],
    split_random=True,
    seed=1575,
    verbosity=0,
    show=False
)

comparisons.fit(data=data,
                run_type="dry_run",
                exclude=["XGBRegressor",
                         "BaggingRegressor",
                         "DecisionTreeRegressor",
                         "ExtraTreeRegressor",
                         "ExtraTreesRegressor",
                         "XGBRFRegressor",
                         "GradientBoostingRegressor",
                         'AdaBoostRegressor',
                         'RandomForestRegressor',
                         'CatBoostRegressor',
                         'HistGradientBoostingRegressor',
                         'RANSACRegressor',
                         "SGDRegressor"
                         ])

# %%

res = comparisons.compare_errors(
    'r2',
    cutoff_val=0.01,
    cutoff_type="greater",
figsize=(8, 10),
    colors=('#f0857d', '#fdcd8f'),
    data=data,
    label_bars=True,
bar_label_kws={"color": 'black', 'label_type':'center'}
)
plt.tight_layout()
maybe_save("results/figures/rev_ml_experiments")

plt.show()

# %%
# With Selected Features
# -------------------------

selected_features = ['H', 'H/C', 'O/C', 'Surface area',
                     'Adsorption_time (min)', 'Ci', 'solution pH',
 'Volume (L)', 'loading (g)', 'Ion Concentration (M)', 'Adsorbent',
 'Feedstock', 'inorganics', 'Anion_type']

data, _ = make_data(
    inputs = selected_features,
    encoding="ohe")

print(data.shape)
# %%

comparisons = MLRegressionExperiments(
    input_features=data.columns.tolist()[0:-1],
    output_features=data.columns.tolist()[-1:],
    split_random=True,
    seed=1575,
    verbosity=0,
    show=False
)

comparisons.fit(data=data,
                run_type="dry_run",
                exclude=["XGBRegressor",
                         "BaggingRegressor",
                         "DecisionTreeRegressor",
                         "ExtraTreeRegressor",
                         "ExtraTreesRegressor",
                         "XGBRFRegressor",
                         "GradientBoostingRegressor",
                    'AdaBoostRegressor',
                    'CatBoostRegressor',
                    'RANSACRegressor',
                    'HuberRegressor',
                         'HistGradientBoostingRegressor',
                         'RandomForestRegressor',
                         ])

# %%
res = comparisons.compare_errors(
    'r2',
    cutoff_val=0.01,
    cutoff_type="greater",
figsize=(8, 10),
    colors=('#f0857d', '#fdcd8f'),
    data=data,
    label_bars=True,
bar_label_kws={"color": 'black', 'label_type':'center'}
)
plt.tight_layout()
maybe_save("results/figures/rev_ml_experiments_selected")

plt.show()