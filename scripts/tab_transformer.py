"""
================
Tab Transformer
================
"""

import os

import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
from ai4water import Model
from ai4water.utils.utils import TrainTestSplit
from ai4water.models.utils import gen_cat_vocab
from ai4water.models import TabTransformer
from utils import make_data

from utils import set_rcParams
from utils import evaluate_model
from utils import regression_plot, residual_plot
from utils import SAVE, NUMERIC_FEATURES, CAT_FEATURES, LABEL

# %%

set_rcParams()

# %%
data, _ = make_data(encoding=None)

print(data.shape)
# %%

splitter = TrainTestSplit(seed=142)

data[NUMERIC_FEATURES] = data[NUMERIC_FEATURES].astype(float)
data[CAT_FEATURES] = data[CAT_FEATURES].astype(str)
data['qe'] = data['qe'].astype(float)


train_data, test_data, _, _ = splitter.split_by_random(data)

# create vocabulary of unique values of categorical features
cat_vocabulary = gen_cat_vocab(data)

train_y = np.log(train_data[LABEL].values)
test_y = np.log(test_data[LABEL].values)

# %%
train_x = [train_data[NUMERIC_FEATURES].values, train_data[CAT_FEATURES].values]
test_x = [test_data[NUMERIC_FEATURES].values, test_data[CAT_FEATURES].values]

#%%
# All Inputs
# ----------------
#
# model = Model (
#     model=TabTransformer(len(NUMERIC_FEATURES),
#                          cat_vocabulary=cat_vocabulary,
#                          hidden_units=16, depth=3,
#                          final_mlp_units= [84, 42]
#                          ),
#     monitor="r2",
#     patience=50
# )

m_path = os.path.join(os.getcwd(), "results", "tab_all_inputs_log")
c_path = os.path.join(m_path, "config.json")
model = Model.from_config_file(c_path)

# %%
#
# model.fit(x=train_x, y= train_y,
#               validation_data=(test_x, test_y),
#               epochs=50)

w_path = os.path.join(m_path, "weights", "weights_030_0.34192.hdf5")
model.update_weights(w_path)

# %%

train_p = model.predict(x=train_x,)

# %%

evaluate_model(train_y, train_p)

# %%


test_p = model.predict(x=test_x,)

# %%
evaluate_model(test_y, test_p)

# %%

# %%
# Selected Inputs
# -----------------

selected_features = ['H', 'H/C', 'O/C', 'Surface area',
                     'Adsorption_time (min)', 'Ci', 'solution pH',
 'Volume (L)', 'loading (g)', 'Ion Concentration (M)', 'Adsorbent',
 'Feedstock', 'inorganics', 'Anion_type']

num_features = [feature for feature in NUMERIC_FEATURES if feature in selected_features]
cat_features = [feature for feature in CAT_FEATURES if feature in selected_features]

# %%

train_x = [train_data[num_features].values, train_data[cat_features].values]
test_x = [test_data[num_features].values, test_data[cat_features].values]

# %%
#
# model = Model(model=TabTransformer(len(num_features), cat_vocabulary,
#                                    hidden_units=16, depth=3,
#                                    final_mlp_units=[84, 42]
#                                    ),
#              # lr=0.0001,
#               )

m_path = os.path.join(os.getcwd(), "results", "tab_selected_inputs_log")
c_path = os.path.join(m_path, "config.json")
model = Model.from_config_file(c_path)

# %%
#
# model.fit(x=train_x, y= train_y,
#               validation_data=(test_x, test_y),
#               epochs=500, verbose=1)

w_path = os.path.join(m_path, "weights", "weights_484_0.34979.hdf5")
model.update_weights(w_path)

# %%
train_p = model.predict(x=train_x)
# %%
evaluate_model(train_y, train_p)

# %%
test_p = model.predict(x=test_x,)
# %%

evaluate_model(test_y, test_p)

# %%

regression_plot(
    train_true=train_y,
    train_pred=train_p,
    test_true=test_y,
    test_pred=test_p,
    label="Adsorption Capacity (mg/g)"
)

if SAVE:
    plt.savefig("results/figures/tab_selected_inputs_log_reg.png", dpi=600, bbox_inches="tight")
plt.show()
#
# # %%

residual_plot(
    train_true=train_y,
    train_prediction=train_p,
    test_true=test_y,
    test_prediction=test_p
)
if SAVE:
    plt.savefig("results/figures/tab_selected_inputs_log_res.png", dpi=600, bbox_inches="tight")
plt.show()
